$(document).ready(function() {
	//Percent Bar Animations
	$('#ten').animate( { height: '10%' }, 2500);
	$('#twenty').animate( { height: '20%' }, 2500);
	$('#thirty').animate( { height: '30%' }, 2500);
	$('#fourty').animate( { height: '40%' }, 2500);
	$('#fifty').animate( { height: '50%' }, 2500);
	$('#sixty').animate( { height: '60%' }, 2500);
	$('#seventy').animate( { height: '70%' }, 2500);
	$('#eighty').animate( { height: '80%' }, 2500);
	$('#ninety').animate( { height: '90%' }, 2500);
	$('#hundred').animate( { height: '100%' }, 2500);
	
	//skill Bar Animations
	$('#html').animate( { height: '95%' }, 2500);
	$('#css').animate( { height: '90%' }, 2500);
	$('#jquery').animate( { height: '75%' }, 2500);
	$('#php').animate( { height: '80%' }, 2500);
	$('#mysql').animate( { height: '70%' }, 2500);

    // Tooltip only Text
    $('.tool-tip').hover(function(){
        // on Hover
        var title = $(this).attr('title');
        $(this).data('tipText', title).removeAttr('title');
        $('<p class="tooltip"></p>')
        .text(title)
        .appendTo('body')
        .fadeIn('slow');
    }, function() {
        // Hover out
        $(this).attr('title', $(this).data('tipText'));
        $('.tooltip').remove();
    }).mousemove(function(e) {
        var mousey = e.pageY + 0;
		var mousex = e.pageX + -25;
        $('.tooltip')
        .css({ top: mousey, left: mousex })
    });
        
});