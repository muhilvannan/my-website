<?php 
    
    include_once('includes/header.php');
   
 ?>

<title>Muhil</title>

<?php 
    
    include_once('includes/header-body.php');
   
 ?>

<?php 
    
    include_once('includes/scripts.php');
   
 ?>

<div class="container">
 	<header>
 	<h1>
		<a href="/">	A Geek's Profile </a>
	</h1>

	<!-- <h2>
		Neelamuhilvannan Mayavannan
	</h2> -->
	</header>
	<div class="shapesContainer">
		<div class="shape transition" id="meDiv">
			<h1> Me </h1>
		</div>

		<div class="shape transition " id="skillsDiv">
			<h1> Skills </h1>
		</div>

		<div class="shape transition" id="contactDiv">
			<h1> Contact </h1>
		</div>

		<div class="shape" id="portfolioDiv">
			<h1> Timeline </h1>	
		</div>
	</div>


	<div id="skills" style="display:none;">
		<div class="close" id="skillsClose"></div>
		<article>

	 	<h2>
			What I Do Best
		</h2>

		<!-- <h2>
			Neelamuhilvannan Mayavannan
		</h2> -->
		

			
		</article>

		
	</div>

	<div id="me" style="display:none;">
		<div class="close" id="meClose"></div>
		<article>

	 	<h2>
			About me
		</h2>

		<!-- <h2>
			Neelamuhilvannan Mayavannan
		</h2> -->

		<p>I am a self professed geek about everything that computes. I am someone who you would find opening up a computer setting up systems and servers. 
		I love talking to people and scoping out what they want from a web system, and how such a system can provide value to their business.
		With a distinction in Computer Science as a undergraduate degree and a masters in Strategic Information Systems complemented with three years experience in systems analysis and development, I am your go to guy if you want a value adding IT strategy in place for your business.
		The timeline of stuff i have done over the years would show you my varied capabilities and achievements </p>
		</article>

		
	</div>

	<div id="portfolio" style="display:none;">
		<div class="close" id="portfolioClose"></div>
		<article>

	 	<h2>
			Timeline of Achievements
		</h2>

		<!-- <h2>
			Neelamuhilvannan Mayavannan
		</h2> -->
		
		<iframe src='http://embed.verite.co/timeline/?source=0Ar7LhOa--4jidHJJdWNQUzk3V3NPZHh6c1FBNDlQcVE&font=Bevan-PotanoSans&maptype=toner&lang=en&height=650&start_at_end=true&start_zoom_adjust=4' width='100%' height='650' frameborder='0'></iframe>
		</article>

		
	</div>


	<div id="contact" style="display:none;">
		
		<div class="close" id="contactClose"></div>
		<article>

	 	<h2>
			Contact me
		</h2>

		<!-- <h2>
			Neelamuhilvannan Mayavannan
		</h2> -->
		</article>
		<p> Drop me a email at 
			<a href="mailto:muhil@outlook.com" > muhil@outlook.com </a>
		</p>

		<p>
			Network with me 
			<a href="http://uk.linkedin.com/in/neelamuhilvannan">
	      
	          <img src="http://www.linkedin.com/img/webpromo/btn_myprofile_160x33.png" width="160" height="33" border="0" alt="View Neela Muhil Vannan Mayavannan's profile on LinkedIn">
	        
	    	</a>	
				
		</p>

		
	</div>

	<footer>
		<h2> Design and coding by Neelamuhilvannan Mayavannan </h2>
	</footer>

</div>




<script>

$(document).ready(function() {
   // other code here

$( document ).tooltip();

$("#meDiv").click(function(){
		$("#me").toggle('slide', function() {
			$(".shapesContainer").toggle('slideRight');
			    
			  });
		
	});
	$("#meClose").click(function(){
		$("#me").toggle('slide', function() {
			$(".shapesContainer").toggle('slideRight');
			    
			  });
		
	});
	$("#portfolioDiv").click(function(){
		$("#portfolio").toggle('slide', function() {
			$(".shapesContainer").toggle('slideRight');
			    
			  });
		
	});
	$("#portfolioClose").click(function(){
		$("#portfolio").toggle('slide', function() {
			$(".shapesContainer").toggle('slideRight');
			    
			  });
		
	});


	$("#skillsDiv").click(function(){
		$("#skills").toggle('slide', function() {
			$(".shapesContainer").toggle('slideRight');
			    
			  });
		
	});
	$("#skillsClose").click(function(){
		$("#skills").toggle('slide', function() {
			$(".shapesContainer").toggle('slideRight');
			    
			  });
		
	});

	$("#contactDiv").click(function(){
		$("#contact").toggle('slide', function() {
			$(".shapesContainer").toggle('slideRight');
			    
			  });
				
	});

	$("#contactClose").click(function(){
		$("#contact").toggle('slide', function() {
			$(".shapesContainer").toggle('slideRight');
			    
			  });
		
	});


});
</script>

<?php 
	include_once('includes/footer.php'); 
?>
